#!/usr/bin/env python

import getopt
import getpass
import logging
import os
import random
import StringIO
import subprocess
import sys
import time

################################################################################
# Constants
################################################################################
SCRIPT_VERSION  = "0.67"
SCRIPT_PATH     = os.path.realpath(__file__)
DEFAULT_CRUX    = "/net/maccoss/vol2/software/bin/crux-pipeline-files/crux/crux_current"
CRUX_DIR        = "/net/maccoss/vol2/software/bin/crux-pipeline-files/crux"
FASTA_DIR       = ("/net/pr/vol1/ProteomicsResource/dbase/UniProt/20160308",
                   "/net/maccoss/vol2/software/bin/crux-pipeline-files/fasta")
PARAM_DIR       = "/net/maccoss/vol2/software/bin/crux-pipeline-files/param"
MSDAPL_USERNAME = "hermie"
MSDAPL_PASSWORD = "hermie2msdapl"
MSDAPL_ADD_URL  = "http://repoman.gs.washington.edu/msdapl_queue/services/msjob/hermie/add"
OPTION_DEFAULTS = {
  "crux_path": DEFAULT_CRUX,
  "bullseye": True,
  "percolator": True,
  "search_engine": "comet",
  "msdapl_id": None,
  "msdapl_name": None,
  "msdapl_species": None,
  "msdapl_instrument": None,
  "msdapl_comment": None,
  "output_dir": None,
  "bullseye_mem": "4.0G",
  "comet_mem": "4.0G",
  "tide-index_mem": "4.0G",
  "tide-search_mem": "4.0G",
  "percolator_mem": "8.0G",
  "msdapl_mem": "1.0G",
  "bullseye_rt": "72:0:0",
  "comet_rt": "120:0:0",
  "tide-index_rt": "72:0:0",
  "tide-search_rt": "72:0:0",
  "percolator_rt": "24:0:0",
  "msdapl_rt": "0:5:0"
}

################################################################################
# Main function
################################################################################
def run_pipeline():
  # Parse arguments and options
  arg_manager = ArgumentManager()
  try:
    parse = arg_manager.parse(sys.argv[1:])
    if parse is not None:
      return parse
  except Exception, error:
    print("%s\n" % error)
    return 2

  # Store values in local variables
  crux_path = arg_manager.options["crux_path"]
  output_dir = arg_manager.options["output_dir"] or get_new_path(
    os.path.abspath("output_") + str(int(time.time())))
  search_engine = arg_manager.options["search_engine"]
  msdapl_id = arg_manager.options["msdapl_id"]
  spectrum_files = arg_manager.args["spectrum_files"]
  fasta_file = arg_manager.args["fasta_file"]
  parameter_file = arg_manager.args["parameter_file"]
  crux_options = " %s " % arg_manager.get_crux_parameters()
  run_bullseye = arg_manager.options["bullseye"]

  # Get filestems and associated ms1 files for spectrum files
  spectrum_filestems = {}
  spectrum_ms1_files = {}
  for spectrum_file in spectrum_files:
    if spectrum_file in spectrum_filestems:
      print("Duplicate spectrum files: '%s'" % spectrum_file)
      return 2
    filestem = os.path.basename(os.path.splitext(spectrum_file)[0])
    if filestem in spectrum_filestems.itervalues():
      print("Multiple spectra files with same filestem '%s'" % filestem)
      return 2
    spectrum_filestems[spectrum_file] = filestem
    if run_bullseye:
      if has_ext(spectrum_file, ".cms1") or has_ext(spectrum_file, ".ms1"):
        print("Spectrum file '%s' is an MS1 file but should be an MS2 file" % spectrum_file)
        return 2
      ms1_file = get_ms1(spectrum_file)
      if not os.path.isfile(ms1_file):
        print("MS1 file for '%s' does not exist (expected '%s')" % (filestem, ms1_file))
        return 2
      spectrum_ms1_files[spectrum_file] = ms1_file

  # Create output directory
  create_dir(output_dir)

  # Set Crux output directory and create it
  crux_output_dir = os.path.join(output_dir, "crux-output")
  create_dir(crux_output_dir)

  # Store escaped variables
  e_fasta_file = escape_str(fasta_file)
  e_parameter_file = escape_str(parameter_file)
  e_crux_output_dir = escape_str(crux_output_dir)

  # Set up logger
  logging.basicConfig(
    format = "[%(asctime)s] %(levelname)s: %(message)s", datefmt = "%Y-%m-%d %H:%M:%S",
    filename = os.path.join(output_dir, "pipeline.log"), level = logging.INFO)
  logging.info("Script version is: %s" % SCRIPT_VERSION)
  logging.info("Current working directory is: %s" % os.getcwd())
  for i in range(0, len(sys.argv)):
    logging.info("Argument %d: %s" % (i, sys.argv[i]))
  logging.info("Output directory is: %s" % output_dir)

  # Display information
  print("Pipeline output directory: %s" % output_dir)
  print
  print("Check the status of this run: %s --status %s" %
        (os.path.basename(__file__), output_dir))
  print("Cancel all jobs started by this run: %s --cancel %s" %
        (os.path.basename(__file__), output_dir))
  print
  print("Submitting jobs...")

  index = None
  index_dir = os.path.join(output_dir, "%s.index" % os.path.basename(os.path.splitext(fasta_file)[0]))
  e_index_dir = escape_str(index_dir)
  if search_engine == "tide":
    create_dir(index_dir)
    # --- Submit tide-index job ---
    index = Job("tide-index", index_dir, arg_manager.options["tide-index_mem"],
                                         arg_manager.options["tide-index_rt"])
    index.commands.append(
      "%s tide-index --parameter-file \"%s\" --output-dir . --fileroot \"\" "
      "%s \"%s\" \"%s\"" %
      (crux_path, e_parameter_file, crux_options, e_fasta_file, e_index_dir))
    index.set_file_dependencies(crux_path, parameter_file, fasta_file)
    logging.info("Submitting tide-index job")
    index.write_and_submit()

  search_jobs = []
  pin_outputs = {}

  # TODO Test paths with [ "'\]
  for spectrum_file in spectrum_files:
    # Create soft link to file for MSDaPl
    subprocess.call(["ln", "-s", spectrum_file, output_dir])
    # Determine subdirectory for this file
    spectrum_filestem = spectrum_filestems[spectrum_file]
    subdir = get_new_path(os.path.join(output_dir, spectrum_filestem))

    bullseye = None
    search = None

    e_spectrum_file = escape_str(spectrum_file)
    e_spectrum_filestem = escape_str(spectrum_filestem)

    if run_bullseye:
      # --- Submit bullseye job ---
      # Output format is forced ms2, to ensure that the following steps can read it.
      # If bullseye fails to produce a bullseye.pid.ms2 file, subsequent steps that use
      # a spectrum file will not run.
      bullseye = Job("bullseye", subdir, arg_manager.options["bullseye_mem"],
                                         arg_manager.options["bullseye_rt"])
      bullseye.commands.append(
        "%s bullseye --parameter-file \"%s\" --output-dir . --fileroot \"\" "
        "--spectrum-format ms2 %s \"%s\" \"%s\"" %
        (crux_path, e_parameter_file,
         crux_options, escape_str(spectrum_ms1_files[spectrum_file]), e_spectrum_file))
      # Rename bullseye outputs for MSDaPl
      bullseye_output_ms2 = os.path.join(crux_output_dir, "%s.pid.ms2" % spectrum_filestem)
      bullseye.commands.append(
        "[ -w bullseye.pid.ms2 ] && mv bullseye.pid.ms2 \"%s\"" %
        (escape_str(bullseye_output_ms2)))
      e_bullseye_params_destination = os.path.join(e_crux_output_dir, "bullseye.params.txt")
      bullseye.commands.append(
        "[ -r bullseye.params.txt ] && ! [ -f \"%s\" ] && cp bullseye.params.txt \"%s\"" %
        (e_bullseye_params_destination, e_bullseye_params_destination))
      bullseye.set_file_dependencies(crux_path, parameter_file, spectrum_file)
      logging.info("Submitting Bullseye job (%s)" % spectrum_filestem)
      bullseye.write_and_submit()
      # Set new spectrum file
      spectrum_file = bullseye_output_ms2
      e_spectrum_file = escape_str(spectrum_file)

    sqt_output_target = os.path.join(e_crux_output_dir, "%s.sqt" % e_spectrum_filestem)
    sqt_output_decoy = os.path.join(e_crux_output_dir, "%s.decoy.sqt" % e_spectrum_filestem)

    if search_engine == "comet":
      pin_outputs[os.path.join(subdir, "comet.target.pin")] = spectrum_filestem
      # --- Submit comet job ---
      search = Job("comet", subdir, arg_manager.options["comet_mem"],
                                    arg_manager.options["comet_rt"])
      search.commands.append(
        "%s comet --parameter-file \"%s\" --output-dir . --fileroot \"\" "
        "--output_sqtfile 1 --output_percolatorfile 1 %s \"%s\" \"%s\"" %
        (crux_path, e_parameter_file, crux_options, e_spectrum_file, e_fasta_file))
      search.commands.append(
        "[ $? -eq 0 ] || { echo \"ERROR: non-zero return code\" 1>&2; exit 1; }\n")
      # Rename comet outputs for MSDaPl
      search.commands.append(
        "[ -w comet.target.sqt ] && mv comet.target.sqt \"%s\"" % sqt_output_target)
      search.commands.append(
        "[ -w comet.decoy.sqt ] && mv comet.decoy.sqt \"%s\"" % sqt_output_decoy)
      e_comet_params_destination = os.path.join(e_crux_output_dir, "comet.params")
      search.commands.append(
        "[ -r comet.params.txt ] && ! [ -f \"%s\" ] && cp comet.params.txt \"%s\"" %
        (e_comet_params_destination, e_comet_params_destination))
      search.commands.append(
        "grep -q \"^database_name=\" \"%s\" || sed -i '1idatabase_name=%s' \"%s\"" %
        (e_comet_params_destination, fasta_file, e_comet_params_destination))
      search.set_file_dependencies(crux_path, parameter_file, spectrum_file, fasta_file)
      logging.info("Submitting Comet job (%s)" % spectrum_filestem)
      search.write_and_submit(bullseye)
    elif search_engine == "tide":
      pin_outputs[os.path.join(subdir, "tide-search.pin")] = spectrum_filestem
      # --- Submit tide-search job ---
      search = Job("tide-search", subdir, arg_manager.options["tide-search_mem"],
                                          arg_manager.options["tide-search_rt"])
      search.commands.append(
        "%s tide-search --parameter-file \"%s\" --output-dir . --fileroot \"\" "
        "--sqt-output T --pin-output T %s --top-match 1 \"%s\" \"%s\"" %
        (crux_path, e_parameter_file, crux_options, e_spectrum_file, e_index_dir))
      search.commands.append(
        "[ $? -eq 0 ] || { echo \"ERROR: non-zero return code\" 1>&2; exit 1; }\n")
      # Rename tide-search outputs for MSDaPl
      search.commands.append(
        "[ -w tide-search.target.sqt ] && mv tide-search.target.sqt \"%s\"" % sqt_output_target)
      search.commands.append(
        "[ -w tide-search.decoy.sqt ] && mv tide-search.decoy.sqt \"%s\"" % sqt_output_decoy)
      search.set_file_dependencies(crux_path, parameter_file, spectrum_file, index_dir)
      logging.info("Submitting tide-search job (%s)" % spectrum_filestem)
      search.write_and_submit(bullseye, index)
    search_jobs.append(search)

  # If not running Percolator, then done
  if not arg_manager.options["percolator"]:
    logging.info("Not running Percolator, done submitting jobs")
    return 0

  # --- Submit percolator job ---
  # Percolator will run using all available *.sqt files.
  # This means it will run as long as any of the searches produced one of these files,
  # even if other searches failed.
  percolator = Job("percolator", crux_output_dir, arg_manager.options["percolator_mem"],
                                                  arg_manager.options["percolator_rt"])
  percolator_input = "make-pin.pin"
  e_percolator_input = escape_str(percolator_input)
  if len(pin_outputs) > 0:
    make_pin_command = "\"%s\" make-pin" % escape_str(SCRIPT_PATH)
    for pin, filestem in pin_outputs.iteritems():
      percolator.commands.append(
        "sed -i 's/^\.\/comet/%s/' \"%s\"" % (filestem, escape_str(pin)))
      make_pin_command += " \"%s\"" % escape_str(pin)
    make_pin_command += " \"%s\"" % e_percolator_input
    percolator.commands.append(make_pin_command)

  percolator.commands.append(
    "%s percolator --parameter-file \"%s\" --output-dir . --fileroot \"\" "
    "--pout-output T %s \"%s\"" %
    (crux_path, e_parameter_file, crux_options, e_percolator_input))
  # Rename percolator outputs for MSDaPl
  percolator_output_xml = os.path.join(crux_output_dir, "combined-results.perc.xml")
  percolator.commands.append(
    "[ -w percolator.pout.xml ] && mv percolator.pout.xml \"%s\"" %
    (escape_str(percolator_output_xml)))
  percolator.set_file_dependencies(crux_path, parameter_file, pin_outputs.keys())
  logging.info("Submitting Percolator job")
  percolator.write_and_submit(search_jobs)

  # If not uploading to MSDaPl, then done
  if msdapl_id is None:
    logging.info("No MSDaPl ID given, done submitting jobs")
    return 0

  # --- Submit MSDaPl upload job ---
  msdapl = Job("msdapl", output_dir, arg_manager.options["msdapl_mem"],
                                     arg_manager.options["msdapl_rt"])
  msdapl_params = {
    "projectId": msdapl_id,
    "dataDirectory": output_dir,
    "submitterName": arg_manager.options["msdapl_name"],
    "targetSpecies": arg_manager.options["msdapl_species"],
    "instrument": arg_manager.options["msdapl_instrument"],
    "comment": arg_manager.options["msdapl_comment"]
  }
  msdapl_param_string = "{%s}" % (",".join(
    ["\"%s\":\"%s\"" % (k, escape_str(str(v))) for k, v in msdapl_params.iteritems() if v]))

  msdapl.commands.append(
    "curl -u %s:%s -X POST -H 'Content-Type: application/json' -d '%s' %s" %
    (MSDAPL_USERNAME, MSDAPL_PASSWORD, msdapl_param_string, MSDAPL_ADD_URL))
  msdapl.set_file_dependencies(percolator_output_xml)
  logging.info("Submitting MSDaPl upload job")
  msdapl.write_and_submit(percolator)
  return 0

################################################################################
# Job
#
# This class is used to represent a job to be submitted to the cluster.
################################################################################
class Job:
  def __init__(self, tool_name, job_dir, mem_requested, runtime):
    self.job_name = "%s_%s_%s" % (getpass.getuser(), tool_name, rand_str(12))
    self.script_path = os.path.join(job_dir, "%s_script.sh" % tool_name)
    self.interpreter = "/bin/bash"
    self.job_dir = job_dir
    self.stdout_log = "%s_stdout.log" % tool_name
    self.stderr_log = "%s_stderr.log" % tool_name
    self.mem_requested = mem_requested
    self.runtime = runtime
    self.file_dependencies_str = ""
    self.commands = []

  # Get the name of a job
  def get_job_name(self, obj):
    if obj is None or isinstance(obj, basestring):
      return obj
    if isinstance(obj, Job):
      return obj.job_name
    raise RuntimeError("Invalid type for get_job_name '%s'" % type(obj))

  # Gets a file dependency string
  def get_file_dependency_string(self, file_dependency):
    return (
      "[ -r \"%s\" ] || "
      "{ echo \"ERROR: Not running script due to missing file: %s\" 1>&2; exit 1; }\n" %
      (escape_str(file_dependency), escape_str(file_dependency)))

  # Sets the file dependencies for this job
  def set_file_dependencies(self, *file_dependency):
    all_files = []
    for file in file_dependency:
      if not hasattr(file, "__iter__"):
        all_files.append(file)
      else:
        all_files.extend(file)
    self.file_dependencies_str = "".join(
      self.get_file_dependency_string(file) for file in all_files)

  # Write a script file for this job
  def write(self):
    create_dir(self.job_dir)
    file = open(self.script_path, "w")
    file.write(
      "#$ -N \"" + escape_str(self.job_name) + "\"\n"
      "#$ -S \"" + escape_str(self.interpreter) + "\"\n"
      "#$ -wd \"" + escape_str(self.job_dir) + "\"\n"
      "#$ -o \"" + escape_str(self.stdout_log) + "\"\n"
      "#$ -e \"" + escape_str(self.stderr_log) + "\"\n"
      "#$ -l m_mem_free=" + self.mem_requested + "\n"
      "#$ -l h_rt=" + self.runtime + "\n"
      "\n"
      ". /etc/profile.d/modules.sh\n"
      "module load modules modules-init modules-gs \\\n"
      "            mpc/0.8.2 mpfr/3.0.0 gmp/5.0.2 gcc/4.7.0\n"
      "\n"
      + self.file_dependencies_str + "\n"
      + "\n".join(self.commands) + "\n"
      "\n"
    )
    file.close()

  # Write a script file for this job, then submit it
  def write_and_submit(self, *job_dependency):
    self.write()
    args = ["qsub"]
    # Build dependency list and add it to arguments
    depend = []
    for job in job_dependency:
      if not hasattr(job, "__iter__"):
        depend.append(job)
      else:
        depend.extend(job)
    depend_str = ",".join(filter(None, tuple(self.get_job_name(job) for job in depend)))
    if depend_str:
      args.extend(["-hold_jid", depend_str])
    # Add script path as final argument and submit
    args.append(self.script_path)
    subprocess.call(args)

################################################################################
# ArgumentManager
#
# This class is used to parse and validate arguments and options
################################################################################
class ArgumentManager:
  def __init__(self):
    self.short_flags = "hvs:c:b:p:e:m:n:x:i:t:o:"
    self.long_flags = [
      "help", "version", "status=", "cancel=", "list-fasta", "list-param",
      "crux-path=", "bullseye=", "percolator=", "search-engine=", "msdapl-id=",
      "msdapl-name=", "msdapl-species=", "msdapl-instrument=", "msdapl-comment=",
      "output-dir=",
      # Memory parameters
      "bullseye-mem=", "comet-mem=", "tide-index-mem=", "tide-search-mem=",
      "percolator-mem=", "msdapl-mem=",
      # Runtime parameters
      "bullseye-rt=", "comet-rt=", "tide-index-rt=", "tide-search-rt=",
      "percolator-rt=", "msdapl-rt=",
      # Crux parameters
      "resolution=", "search-enzyme-number=",
      "varmod1=", "varmod2=", "varmod3=", "varmod4=", "varmod5=", "varmod6=",
      "A=", "B=", "C=", "D=", "E=", "F=", "G=", "H=", "I=", "J=", "K=", "L=", "M=",
      "N=", "O=", "P=", "Q=", "R=", "S=", "T=", "U=", "V=", "W=", "X=", "Y=", "Z=",
      "tryptic", "semitryptic", "noenzyme", "exact-match"
    ]
    self.options = OPTION_DEFAULTS or {}
    self.args = {"spectrum_files": [], "fasta_file": "", "parameter_file": ""}
    self.status_manager = StatusManager()
    self.keyword_manager = KeywordManager(FASTA_DIR, PARAM_DIR)
    self.extra_crux_params = {}

  # Gets the usage statement
  def get_usage(self):
    return(
      "Usage: " + os.path.basename(__file__) + " [options] "
      "<spectrum file>+ <FASTA file> <parameter file>\n"
      "\n"
      "Commands:\n"
      "  --help (-h) - Displays this message.\n"
      "  --version (-v) - Displays the version number of this script.\n"
      "  --status (-s) <dir> - Displays the status of jobs for a directory.\n"
      "  --cancel (-c) <dir> - Cancels jobs for a directory.\n"
      "  --list-fasta - "
      "Displays a list of FASTA keywords available for this script.\n"
      "  --list-param - "
      "Displays a list of parameter file keywords available for this script.\n"
      "\n"
      "Options:\n"
      "  --crux-path <path> - "
      "Specify the path to the Crux executable to be used.\n"
      "  --bullseye (-b) <T|F> - "
      "Run Hardklor and Bullseye. Default T\n"
      "  --percolator (-p) <T|F> - "
      "Run Percolator. Must be true for loading results into MSDaPl. Default T\n"
      "  --search-engine (-e) <comet|tide> - "
      "The search engine to run. Default comet\n"
      "  --msdapl-id (-m) <id> - "
      "The number of the MSDaPl project to load final results into. Default none\n"
      "  --msdapl-name (-n) <name> - "
      "The submitter's username for MSDaPl. Default none\n"
      "  --msdapl-species (-x) <id> - "
      "The taxonomy ID of the target species for MSDaPl. Default none\n"
      "  --msdapl-instrument (-i) <instrument> - "
      "The name of the instrument used to acquire data for MSDaPl. Default none\n"
      "  --msdapl-comment (-t) <comment> - "
      "Comments to be used when uploading data to MSDaPl. Default none\n"
      "  --output-dir (-o) <dir> - "
      "The directory where results files will be outputted. Default output_<timestamp>\n"
      "  --bullseye-mem <value>, --comet-mem <value>, "
      "--tide-index-mem <value>, --tide-search-mem <value>, --percolator-mem <value>, "
      "--msdapl-mem <value> - "
      "How much memory to request for a command's job.\n"
      "  --bullseye-rt <value>, --comet-rt <value>, "
      "--tide-index-rt <value>, --tide-search-rt <value>, --percolator-rt <value>, "
      "--msdapl-rt <value> - "
      "Specify estimated runtime for a command's job in format H:M:S.\n"
    )

  # Prints the usage statement
  def print_usage(self):
    print("%s\n" % self.get_usage())

  # Parses a single option and its value; returns 0 if some command is to be executed
  def parse_option(self, option, value):
    if option in ("-h", "--help"):
      self.print_usage()
      return 0
    elif option in ("-v", "--version"):
      print("Version %s\n" % SCRIPT_VERSION)
      return 0
    elif option in ("-s", "--status"):
      self.status_manager.print_status(value)
      return 0
    elif option in ("-c", "--cancel"):
      self.status_manager.cancel(value)
      self.request_cancel = value
      return 0
    elif option == "--list-fasta":
      self.keyword_manager.print_fasta_list()
      return 0
    elif option == "--list-param":
      self.keyword_manager.print_param_list()
      return 0
    elif option == "--crux-path":
      new_path = value
      if "/" not in value:
        alt_path = os.path.join(CRUX_DIR, value)
        if os.path.isfile(alt_path):
          new_path = alt_path
      self.options["crux_path"] = os.path.abspath(new_path)
    elif option in ("-b", "--bullseye"):
      self.options["bullseye"] = to_bool(value)
    elif option in ("-p", "--percolator"):
      self.options["percolator"] = to_bool(value)
    elif option in ("-e", "--search-engine"):
      if not value.lower() in ("comet", "tide"):
        raise RuntimeError("Invalid value for %s '%s'" % (option, value))
      self.options["search_engine"] = value.lower()
    elif option in ("-m", "--msdapl-id"):
      if not value.isdigit():
        raise RuntimeError("Invalid value for %s '%s'" % (option, value))
      self.options["msdapl_id"] = int(value)
    elif option in ("-n", "--msdapl-name"):
      self.options["msdapl_name"] = value
    elif option in ("-x", "--msdapl-species"):
      if not value.isdigit():
        raise RuntimeError("Invalid value for %s '%s'" % (option, value))
      self.options["msdapl_species"] = int(value)
    elif option in ("-i", "--msdapl-instrument"):
      self.options["msdapl_instrument"] = value
    elif option in ("-t", "--msdapl-comment"):
      self.options["msdapl_comment"] = value
    elif option in ("-o", "--output-dir"):
      self.options["output_dir"] = os.path.abspath(value)
    # Memory parameters
    elif option == "--bullseye-mem":
      self.options["bullseye_mem"] = value
    elif option == "--comet-mem":
      self.options["comet_mem"] = value
    elif option == "--tide-index-mem":
      self.options["tide-index_mem"] = value
    elif option == "--tide-search-mem":
      self.options["tide-search_mem"] = value
    elif option == "--percolator-mem":
      self.options["percolator_mem"] = value
    elif option == "--msdapl-mem":
      self.options["msdapl_mem"] = value
    # Runtime parameters
    elif option == "--bullseye-rt":
      self.options["bullseye_rt"] = value
    elif option == "--comet-rt":
      self.options["comet_rt"] = value
    elif option == "--tide-index-rt":
      self.options["tide-index_rt"] = value
    elif option == "--tide-search-rt":
      self.options["tide-search_rt"] = value
    elif option == "--percolator-rt":
      self.options["percolator_rt"] = value
    elif option == "--msdapl-mem":
      self.options["msdapl_rt"] = value
    # Crux parameters
    elif option == "--resolution":
      self.extra_crux_params["resolution"] = value
    elif option == "--search-enzyme-number":
      self.extra_crux_params["search_enzyme_number"] = value
    elif option.startswith("--varmod") and len(option) == 9 and option[8].isdigit():
      mod_num = option[8]
      self.extra_crux_params["variable_mod%c" % mod_num] = value
    elif option.startswith("--") and len(option) == 3 and option[2].isupper():
      comet_static_mod_params = {
        "A": "alanine", "B": "user_amino_acid", "C": "cysteine", "D": "aspartic_acid",
        "E": "glutamic_acid", "F": "phenylalanine", "G": "glycine", "H": "histidine",
        "I": "isoleucine", "J": "user_amino_acid", "K": "lysine", "L": "leucine",
        "M": "methionine", "N": "asparagine", "O": "ornithine", "P": "proline",
        "Q": "glutamine", "R": "arginine", "S": "serine", "T": "threonine",
        "U": "user_amino_acid", "V": "valine", "W": "tryptophan", "X": "user_amino_acid",
        "Y": "tyrosine", "Z": "user_amino_acid"}
      residue = option[2]
      self.extra_crux_params["add_%c_%s" % (residue, comet_static_mod_params[residue])] = value
    elif option == "--tryptic":
      self.extra_crux_params["search_enzyme_number"] = "1"
      self.extra_crux_params["num_enzyme_termini"] = "2"
    elif option == "--semitryptic":
      self.extra_crux_params["search_enzyme_number"] = "1"
      self.extra_crux_params["num_enzyme_termini"] = "1"
    elif option == "--noenzyme":
      self.extra_crux_params["search_enzyme_number"] = "0"
      self.extra_crux_params["num_enzyme_termini"] = "1"
    elif option == "--exact-match":
      self.extra_crux_params["exact-match"] = "T"
    else:
      raise RuntimeError("Something went wrong with '%s'" % option)

  # Gets Crux parameters as a string
  def get_crux_parameters(self):
    return " ".join(
      "--%s \"%s\"" % (k, escape_str(v)) for k, v in self.extra_crux_params.iteritems())

  # Parses pipeline parameters from parameter file, in format: #$ <flag> [value]
  def parse_pipeline_params_from_file(self, param_file):
    args = []
    file = open(param_file, "r")
    for line in file:
      line = line.strip()
      if not line.startswith("#$"):
        continue
      line = line[2:].strip()
      if not line:
        continue
      pieces = line.split(" ", 1)
      args.append(pieces[0].strip())
      if len(pieces) == 1 or not pieces[1]:
        continue
      value = pieces[1].strip()
      if len(value) > 1:
        if value[0] == "\"" and value[-1] == "\"":
          value = unescape_str(value[1:-1])
        elif value[0] == "'" and value[-1] == "'":
          value = unescape_str(value[1:-1], "'")
      args.append(value)
    file.close()
    try:
      parsed_options, parsed_args = getopt.gnu_getopt(
        args, self.short_flags, self.long_flags)
    except:
      return
    for option, value in parsed_options:
      self.parse_option(option, value)

  # Parses and validates the arguments
  def parse(self, args):
    try:
      parsed_options, parsed_args = getopt.gnu_getopt(args, self.short_flags, self.long_flags)
    except getopt.GetoptError, error:
      raise RuntimeError("%s\n\n%s" % (error, self.get_usage()))

    sufficient_args = len(parsed_args) >= len(self.args)
    if sufficient_args:
      # Process spectrum file argument(s)
      # TODO check if directory and use all ms2/cms2/other?
      for value in parsed_args[:-2]:
        spectrum_file = os.path.abspath(value)
        if not os.path.isfile(spectrum_file):
          raise RuntimeError("Spectrum file does not exist: '%s'" % spectrum_file)
        self.args["spectrum_files"].append(spectrum_file)

      # Process FASTA argument
      parsed_fasta = parsed_args[-2]
      resolved_fasta = self.keyword_manager.get_fasta_file(parsed_fasta)
      if not resolved_fasta:
        raise RuntimeError("FASTA does not exist: '%s'" % parsed_fasta)
      self.args["fasta_file"] = os.path.abspath(resolved_fasta)

      # Process parameter file argument
      parsed_param = parsed_args[-1]
      resolved_param = self.keyword_manager.get_param_file(parsed_param)
      if not resolved_param:
        raise RuntimeError("Parameter file does not exist: '%s'" % parsed_param)
      self.args["parameter_file"] = os.path.abspath(resolved_param)

      # Parse any options from parameter file before command line
      self.parse_pipeline_params_from_file(self.args["parameter_file"])

    # Parse from command line
    for option, value in parsed_options:
      parse = self.parse_option(option, value)
      if parse is not None:
        return parse

    # Check for correct number of arguments
    if not sufficient_args:
      raise RuntimeError("Found %d arguments, but was expecting at least %d.\n\n%s" %
        (len(parsed_args), len(self.args), self.get_usage()))

    # Verify Crux path
    if not os.path.isfile(self.options["crux_path"]):
      raise RuntimeError("Crux executable not found at '%s'" % self.options["crux_path"])

    # Verify output directory
    if self.options["output_dir"] and os.path.exists(self.options["output_dir"]):
      raise RuntimeError("Directory already exists '%s'" % value)

################################################################################
# StatusManager
#
# This class is used to get the status of jobs and cancel jobs.
################################################################################
class StatusManager:
  def __init__(self):
    self.spectrum_jobs = ("bullseye", "comet", "tide-index", "tide-search")

  # Read lines from a script file to find the script name
  def get_job_name(self, script_file):
    name = None
    file = open(script_file, "r")
    try:
      for line in iter(file):
        if line.startswith("#$ -N "):
          name = line[5:].strip()
          if name.startswith("\"") and name.endswith("\""):
            name = name[1:-1]
          break
    finally:
      if file is not None:
        file.close()
    return name

  # Return the status of a job, given its name
  def get_job_status(self, job_name):
    # qstat -j <job>, find job_number
    qstat = subprocess.Popen(["qstat", "-j", job_name], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = qstat.communicate()
    if stderr.startswith("Following jobs do not exist"):
      return "-"
    stdout_buffer = StringIO.StringIO(stdout)
    job_number = None
    error_reason = None
    for line in iter(stdout_buffer.readline, ""):
      if not ":" in line:
        continue
      key, value = line.split(":", 1)
      key = key.strip()
      value = value.strip()
      if key == "job_number":
        job_number = value
      elif key == "error_reason":
        error_reason = value
    stdout_buffer.close()
    if job_number is None:
      return "(internal error)"
    # qstat (no arguments), find the line for this job, and parse the state
    qstat = subprocess.Popen(["qstat"], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = qstat.communicate()
    stdout_buffer = StringIO.StringIO(stdout)
    state = "-"
    for line in iter(stdout_buffer.readline, ""):
      fields = line.split()
      if len(fields) < 5:
        continue
      elif fields[0] == job_number:
        state = fields[4]
    stdout_buffer.close()
    if error_reason:
      state = "%s, %s" % (state, error_reason)
    return state

  # Prints a string describing a job's status, given its script file if it exists
  def print_job_status(self, job_description, script_file):
    if not os.path.isfile(script_file):
      return
    print("%s: %s" % (job_description, self.get_job_status(self.get_job_name(script_file))))

  # Prints the status for a complete pipeline run, given the output directory
  def print_status(self, dir):
    devnull = open(os.devnull, "wb")
    which = subprocess.Popen(["which", "qstat"], shell=False, stdout=devnull, stderr=devnull)
    which.communicate()
    if which.returncode == 1:
      print("qstat not found, are you logged into the head node?")
      return
    for subdir in os.walk(dir).next()[1]:
      subpath = os.path.join(dir, subdir)
      if subdir == "crux-output":
        continue
      print("========== %s ==========" % subdir)
      for job in self.spectrum_jobs:
        self.print_job_status(job, os.path.join(subpath, "%s_script.sh" % job))
      print
    print("====================")
    job = "percolator"
    self.print_job_status(job, os.path.join(dir, "crux-output", "%s_script.sh" % job))
    job = "msdapl"
    self.print_job_status(job, os.path.join(dir, "%s_script.sh" % job))
    print

  # Cancels jobs, given their associated script files
  def cancel_jobs(self, script_files):
    qdel_args = list(self.get_job_name(file) for file in script_files if os.path.isfile(file))
    if not qdel_args:
      return
    qdel_args.insert(0, "qdel")
    qdel = subprocess.Popen(qdel_args, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = qdel.communicate()
    if stderr:
      print(stderr)
    stdout_buffer = StringIO.StringIO(stdout)
    for line in iter(stdout_buffer.readline, ""):
      line = line.strip()
      if line.endswith("does not exist"):
        continue
      print line
    stdout_buffer.close()

  # Cancels all jobs started by a pipeline run, given the output directory
  def cancel(self, dir):
    devnull = open(os.devnull, "wb")
    which = subprocess.Popen(["which", "qdel"], shell=False, stdout=devnull, stderr=devnull)
    which.communicate()
    if which.returncode == 1:
      print("qdel not found, are you logged into the head node?")
      return
    for subdir in os.walk(dir).next()[1]:
      subpath = os.path.join(dir, subdir)
      if subdir != "crux-output":
        self.cancel_jobs(os.path.join(subpath, "%s_script.sh" % job) for job in self.spectrum_jobs)
    self.cancel_jobs(
      (os.path.join(dir, "crux-output/percolator_script.sh"), os.path.join(dir, "msdapl_script.sh")))

################################################################################
# KeywordManager
#
# This class is used to manage FASTA and parameter file keywords.
################################################################################
class KeywordManager:
  def __init__(self, fasta_dirs, param_dirs):
    self.fasta_keywords = self.init_dirs(fasta_dirs)
    self.param_keywords = self.init_dirs(param_dirs)

  # Read a directory and build a dictionary of keywords mapped to their associated files
  # For any link to a file, any non-links pointing to that file are not keywords
  def init_dirs(self, dirs):
    keywords = {}
    link_keywords = {}
    dir_list = []
    if isinstance(dirs, basestring):
      dir_list.append(dirs)
    else:
      dir_list.extend(dirs)
    for dir in dir_list:
      for result in os.walk(dir):
        for file in result[2]:
          if "~" in file or file.startswith("."):
            continue
          keyword = file
          value = os.path.join(result[0], file)
          if os.path.islink(value):
            value = os.path.join(os.path.dirname(value), os.readlink(value))
            link_keywords[keyword.lower()] = value
            keywords = dict((k, v) for (k, v) in keywords.iteritems() if v != value)
          elif not value in link_keywords.itervalues():
            keywords[keyword.lower()] = value
    return dict(keywords.items() + link_keywords.items())

  # Return the file, or file associated with it if it is a keyword; otherwise None
  def resolve(self, file_or_keyword, keyword_dictionary):
    if os.path.isfile(file_or_keyword):
      return file_or_keyword
    try:
      return keyword_dictionary[file_or_keyword.lower()]
    except:
      return None

  # Return the FASTA file, or associated FASTA file if it is a keyword; otherwise None
  def get_fasta_file(self, fasta):
    return self.resolve(fasta, self.fasta_keywords)

  # Return the parameter file, or associated parameter file if it is a keyword; otherwise None
  def get_param_file(self, param):
    return self.resolve(param, self.param_keywords)

  # Print a list of keywords and their associated values
  def print_list(self, items):
    for keyword, file in items:
      print("\"%s\" -> %s" % (keyword, file))

  # Print a list of FASTA keywords and their associated values
  def print_fasta_list(self):
    self.print_list(self.fasta_keywords.iteritems())

  # Print a list of parameter keywords and their associated values
  def print_param_list(self):
    self.print_list(self.param_keywords.iteritems())

################################################################################
# PinMaker
#
# Merge all of the .pin outputs from Comet, keeping only the top matches,
# removing the "Mass" column, and adding the "ExpMass" and "CalcMass" columns
# after the "ScanNr" column
################################################################################
class PinMaker:
  def __init__(self, paths):
    self.input_files = []
    if isinstance(paths, basestring):
      self.input_files.append(paths)
    else:
      self.input_files.extend(paths)

  # Write out a pin file from all files that have been added with add_input
  def run(self, path):
    file_out = open(path, "w")
    id_idx = -1
    score_idx = -1
    scan_nr_idx = -1
    exp_mass_idx = -1
    dm_idx = -1
    for file_idx, input in enumerate(self.input_files):
      best = {} # id -> (line, score)
      file_in = open(input, "r")
      for line_idx, line in enumerate(file_in):
        line = line.strip()
        if not line:
          continue
        pieces = line.split("\t")
        if line_idx == 0:
          if file_idx == 0:
            headers = []
            for i, v in enumerate(pieces):
              header = v.lower()
              if header == "mass":
                exp_mass_idx = i
                continue
              headers.append(v)
              if header == "id":
                id_idx = i
              if header == "xcorr":
                score_idx = i
              elif header == "scannr":
                scan_nr_idx = i
                headers.append("ExpMass") # Add ExpMass and CalcMass after ScanNr
                headers.append("CalcMass")
              elif header == "dm":
                dm_idx = i
            file_out.write("%s\n" % "\t".join(headers))
          continue
        cur_id = pieces[0]
        cur_score = pieces[score_idx]
        if cur_id not in best or cur_score > best[cur_id][1]:
          best[cur_id] = (line, cur_score)
      file_in.close()
      for line in best.itervalues():
        write_line = ""
        pieces = line[0].split("\t")
        for i, v in enumerate(pieces):
          if i == exp_mass_idx: # Don't write the Mass value
            continue
          elif write_line:
            write_line += "\t"
          write_line += v
          if i == scan_nr_idx and exp_mass_idx >= 0: # Write ExpMass and CalcMass after ScanNr
            exp_mass = float(pieces[exp_mass_idx])
            calc_mass = float(exp_mass) / (1 + float(pieces[dm_idx]))
            write_line += "\t%.6f\t%.6f" % (exp_mass, calc_mass)
        write_line += "\n"
        file_out.write(write_line)
    file_out.close()
    return 0

################################################################################
# Miscellaneous Functions
################################################################################

# Return a string of random alphanumeric characters
def rand_str(len):
  return "".join(random.sample(
    "abcdefghijklmnopqrstuvwxyz"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "1234567890", len))

# Convert a string to boolean
def to_bool(s):
  if s.lower() in ["t", "true", "yes", "1"]:
    return True
  if s.lower() in ["f", "false", "no", "0"]:
    return False
  raise RuntimeError("Invalid value for boolean '%s'" % s)

# Return a string with the specified characters escaped
def escape_str(s, chars = "\""):
  if s is None or chars is None:
    return s
  if "\\" in chars:
    s = s.replace("\\", "\\\\")
  for char in set(chars):
    if char != "\\":
      s = s.replace(char, "\\%c" % char)
  return s

# Return a string with the specified characters unescaped
def unescape_str(s, chars = "\""):
  if s is None or chars is None:
    return s
  for char in set(chars):
    s = s.replace("\\%c" % char, char)
  return s

# Create directory if it doesn't exist
def create_dir(dir):
  if not os.path.exists(dir):
    os.makedirs(dir)

# Appends numbers to the path until we get a path that doesn't exist
def get_new_path(target_path):
  suffix = 1
  path = target_path
  while os.path.exists(path):
    path = "%s_%s" % (target_path, suffix)
    suffix += 1
  return path

# Check if the path has the specified extension
def has_ext(path, ext):
  return path.lower().endswith(ext.lower())

# Returns the corresponding MS1 file for the MS2 file
def get_ms1(ms2_path):
  if has_ext(ms2_path, ".ms2") or has_ext(ms2_path, ".cms2"):
    return "%s1" % ms2_path[:-1]
  return ms2_path

# Run the main function
if __name__ == "__main__":
  if len(sys.argv) > 1 and sys.argv[1] == "make-pin":
    if len(sys.argv) < 4: # script make-pin <input>+ <output>
      print "Not enough inputs for make-pin"
      sys.exit(1)
    inputs = sys.argv[2:-1]
    output = sys.argv[-1]
    print "Creating pin file %s from input files: %s" % (output, ", ".join(inputs))
    pin_maker = PinMaker(inputs)
    sys.exit(pin_maker.run(output))
  else:
    sys.exit(run_pipeline())

