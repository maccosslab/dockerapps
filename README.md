# Dockerapps #

Example of build process:
```bash
> docker build . -t skyline64:testbuild.20180221.rc0
> docker tag skyline64:testbuild.20180221.rc0 atkeller/skyline64:testbuild.20180221.rc0
> docker push atkeller/skyline64:testbuild.20180221.rc0
```
