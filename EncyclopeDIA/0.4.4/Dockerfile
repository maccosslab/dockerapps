FROM openjdk:8-jre

SHELL ["/bin/bash", "-c"]

# Install python and boto3 for s3 wrap, openMP for percolator v3
RUN apt-get update && apt-get install -y --no-install-recommends python python-pip libgomp1 && pip install boto3

# Add a non-root user to run the app
RUN groupadd -r appuser && useradd -r -g appuser appuser

# Create conda and python setup directories and a working directory
RUN mkdir /app /app/java /app/wd /app/scratch && chmod -R 777 /app/wd && chmod -R 777 /app/scratch
ENV SCRATCH_DIR=/app/scratch

WORKDIR /app

COPY s3_wrap_encyclopedia.py /app
COPY encyclopedia-0.4.4-executable.jar /app/java

WORKDIR /app/wd

USER appuser

ENTRYPOINT ["python", "/app/s3_wrap_encyclopedia.py", "java", "-jar", "../java/encyclopedia-0.4.4-executable.jar"]

##### Running the container #####
# the container can be run with a directory on the host system mounted to /app/wd/mount
# to do this, run with the arguments -v and -u specified, for example:
# docker run -u 1001:100 -i -t -v /mnt/host_pc/test_mount:/app/wd/mount diapipeline /bin/bash
# -v : <host_directory>:/app/wd/mount
#    : Make sure to pass the absolute path for the host directory! Or else Docker will create its own volume
# -u : <userID>:<groupID>
#    : this should match the uid and gid for the mounted directory on the host system
#
# if the current user owns the host mount point, you can do this:
# docker run -u `id -u $USER`:`id -g $USER` -i -t -v /mnt/host_pc/test_mount:/app/wd/mount diapipeline /bin/bash
